﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unosquare.RaspberryIO;

namespace PiSpy {
	class MotionSensor {

		public static bool ReadMotion() {
			var isOn = Pi.Gpio.Pin04.ReadValue();
			if (Pi.Gpio.Pin07.ReadValue() == 0) {
				Console.WriteLine("No Motion");
				return false;
			}
			else
				Console.WriteLine("Motion Detected!");
			return true;
		}
	}
}
