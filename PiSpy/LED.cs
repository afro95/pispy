﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Gpio;

namespace PiSpy {
	class LED {

		public static void toggleLED() {
			var blinkingPin = Pi.Gpio[25];
			blinkingPin.PinMode = GpioPinDriveMode.Output;
			var isOn = false;
			for (var i = 0; i < 20; i++) {
				isOn = !isOn;
				blinkingPin.Write(isOn);
				System.Threading.Thread.Sleep(500);
			}
		}
	}
}
