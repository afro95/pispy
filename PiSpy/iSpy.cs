﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using PiSpy;
using Unosquare.RaspberryIO;

namespace PiSpy {
    class iSpy {

        public void RecordMotion() {

            while (true) {
                if (MotionSensor.ReadMotion()) {
                    Camera.CaptureImage(1920, 1080);
                }
                Console.WriteLine("Wait 2...");
                System.Threading.Thread.Sleep(2000);
            }
        }

        public void RecordMotion(double length) {
            var later = DateTime.Now.AddMinutes(length);
            while (DateTime.Now <= later) {
                if (MotionSensor.ReadMotion()) {
                    Camera.CaptureImage(1920, 1080);
                }
                System.Threading.Thread.Sleep(2000);
            }
            Console.WriteLine("Disarmed....");
        }

        public bool isOccupied() {

            Thread.Sleep(1000);
            Console.WriteLine("Checking Occupancy...");
            return MotionSensor.ReadMotion();
        }

        public bool isOccupied(int seconds) {
            var later = DateTime.Now.AddSeconds(seconds);

            int yes = 0;
            int no = 0;

            while (DateTime.Now <= later) {
                if (MotionSensor.ReadMotion()) {
                    yes++;
                }
                else {
                    no++;
                }
                Thread.Sleep(1000);
            }

            if (yes >= no) {
                Console.WriteLine("OCCUPIED" + $" YES: {yes} NO: {no}");
                return true;
            }
            else {
                Console.WriteLine("NOT OCCUPIED" + $" YES: {yes} NO: {no}");
                return false;
            }
        }

        public void simpleMoniter() {
            Console.WriteLine("Initializing...");
            while (true) {
                Thread.Sleep(1000);
                Console.WriteLine("Start Scan..");
                while (isOccupied(10)) {
                    Console.WriteLine("Sleeping 1 min");
                    Thread.Sleep(60000);
                }

                var later = DateTime.Now.AddMinutes(.5);

                while (DateTime.Now <= later) {
                    Console.WriteLine("Armed for 30 seconds....");
                    this.RecordMotion(.5);
                }
            }
        }

        public void testMoniter() {
            Console.WriteLine("Initializing...");
            Thread.Sleep(1000);

            while (true) {
                Console.WriteLine("Start Scan..");
                while (isOccupied(10)) {
                    Console.WriteLine("Sleeping 1 min");
                    Thread.Sleep(10000);
                }
            }
        }

    }

}