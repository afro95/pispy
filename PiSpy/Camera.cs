﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Camera;

namespace PiSpy {
	class Camera {

		public static void CaptureImage(int width, int height) {
			var pictureBytes = Pi.Camera.CaptureImageJpeg(width, height);
			var targetPath = "Captures/capture";

			File.WriteAllBytes(targetPath + 
				string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now) 
				+ ".jpg", pictureBytes);

			Console.WriteLine($"Took picture -- Byte count: {pictureBytes.Length}");
		}


		public static void CaptureVideo() {
			// Setup our working variables
			var videoByteCount = 0;
			var videoEventCount = 0;
			var startTime = DateTime.UtcNow;

			// Configure video settings
			var videoSettings = new CameraVideoSettings() {
				CaptureTimeoutMilliseconds = 300,
				CaptureDisplayPreview = false,
				ImageFlipVertically = true,
				CaptureExposure = CameraExposureMode.Night,
				CaptureWidth = 1920,
				CaptureHeight = 1080
			};

			try {
				// Start the video recording
				Pi.Camera.OpenVideoStream(videoSettings,
					onDataCallback: (data) => { videoByteCount += data.Length; videoEventCount++; File.WriteAllBytes("video.h264", data); },
					onExitCallback: null);

				// Wait for user interaction
				startTime = DateTime.UtcNow;
				//var end = DateTime.Now.AddMilliseconds(5000);
				//while (DateTime.UtcNow < end) {

				//}
				

			}
			catch (Exception ex) {
				Console.WriteLine($"{ex.GetType()}: {ex.Message}");
			}
			finally {
				// Always close the video stream to ensure raspivid quits
				Pi.Camera.CloseVideoStream();

				// Output the stats
				var megaBytesReceived = (videoByteCount / (1024f * 1024f)).ToString("0.000");
				var recordedSeconds = DateTime.UtcNow.Subtract(startTime).TotalSeconds.ToString("0.000");
				Console.WriteLine($"Capture Stopped. Received {megaBytesReceived} Mbytes in {videoEventCount} callbacks in {recordedSeconds} seconds");
			}
		}


	}
}
